import React from 'react'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'

const SkillsSection = () => (
    <ListGroup variant="flush">
        <ListGroup.Item>
            <Row className='h4'>Technical</Row>
            <ListGroup variant="flush">
                <ListGroup.Item>
                <Row>
                    <Col lg={2}>Proficient:</Col>
                    <Col>Swift, Objective-C, iOS Development, Mobile UI/UX Design, HTML, CSS, Sass, Markdown, Jekyll, Gatsby, Cache (MUMPS)</Col>
                </Row>  
                </ListGroup.Item>
                <ListGroup.Item>
                <Row>
                    <Col lg={2}>Familiar:</Col>
                    <Col>Kotlin, Java, Android Development, TypeScrpit, JavaScript, React Native, RESTful API Design, Python, C# & .NET,  Adobe XD, Inkscape, GIMP</Col>
                </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                <Row>
                    <Col lg={2}>Dabbled:</Col>
                    <Col>C, Scala, Node.js, Visual Basic, Google Analytics, Drupal</Col>
                </Row>
                </ListGroup.Item>
            </ListGroup>
        </ListGroup.Item>
        <ListGroup.Item>
        <Row className='h4'>Leadership</Row>
            <ListGroup variant="flush">
                <ListGroup.Item>
                <Row className='p'>Coaching, mentoring, project managment, colloaborative problem solving</Row>
                </ListGroup.Item>
            </ListGroup>
        </ListGroup.Item>
        
    </ListGroup>
)

export default SkillsSection