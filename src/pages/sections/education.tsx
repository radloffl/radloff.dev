import React from 'react'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'

const EducationSection = () => (
    <ListGroup variant="flush">
        <ListGroup.Item>
        <Row className='h4'>University of Northern Iowa</Row>
        <Row className='h6'>Cedar Falls, IA</Row>
        <Row className='p'>Bachelor of Science in Computer Science, GPA 3.9/4.0</Row>
        <Row>
            <ul>
            <li>Emphasis in Software Engineering</li>
            </ul>
        </Row>
        </ListGroup.Item>
    </ListGroup>
)

export default EducationSection