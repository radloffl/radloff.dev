import React from 'react'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'

const ReferencesSection = () => (
    <ListGroup variant="flush">
        <ListGroup.Item>
        <Row className='h4'>Richard Happe</Row>
        <Row className='h6'>Supervisor, Epic Systems</Row>
        <Row className='p'>
            <a href='mailto:richhappe@gmail.com'>richhappe@gmail.com</a>
        </Row>
        <Row className='p'>
            <a href='tel:+15708727773'>(570) 872-7773</a>
        </Row>
        </ListGroup.Item>
        <ListGroup.Item>
        <Row className='h4'>Kamal Calloway</Row>
        <Row className='h6'>Founder, Madison Roots</Row>
        <Row>
            <a href='mailto:kamal.calloway@gmail.com'>kamal.calloway@gmail.com</a>
        </Row>
        <Row className='p'>
            <a href='tel:+13124985455'>(312) 498-5455</a>
        </Row>
        </ListGroup.Item>
    </ListGroup>
)

export default ReferencesSection