import React from "react"
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'

const ExperienceSection = () => (
    <ListGroup variant="flush">
        <ListGroup.Item>
            <Row className='h3'>Epic Systems Corporation</Row>
            <Row className='h6'>Verona, WI</Row>
            <ListGroup variant="flush">
                <ListGroup.Item>
                <Row className='h4'>Senior Software Systems Developer</Row>
                <Row className='h5'>May 2023-Present</Row>
                </ListGroup.Item>
                <ListGroup.Item>
                <Row className='h4'>Software Developer II</Row>
                <Row className='h5'>May 2022-May 2023</Row>
                </ListGroup.Item>
                <ListGroup.Item>
                <Row className='h4'>Software Developer & Team Lead</Row>
                <Row className='h5'>June 2020-May 2022</Row>
                </ListGroup.Item>
                <ListGroup.Item>
                <Row className='h4'>Software Developer</Row>
                <Row className='h5'>May 2018-Present</Row>
                </ListGroup.Item>
                <ListGroup.Item>
                <Row className='h4'>Software Developer Intern</Row>
                <Row className='h5'>January 2016-August 2016 & May 2017-August 2017</Row>
                </ListGroup.Item>
                
            </ListGroup>
            </ListGroup.Item>
            <ListGroup.Item>
            <Row className='h3'>Jack Henry & Associates</Row>
            <Row className='h6'>Remote (Cedar Falls, IA)</Row>
            <ListGroup variant="flush">
                <ListGroup.Item>
                <Row className='h4'>Software Developer Intern</Row>
                <Row className='h5'>August 2017-May 2018</Row>
                </ListGroup.Item>
            </ListGroup>
            </ListGroup.Item>
            <ListGroup.Item>
            <Row className='h3'>University of Northern Iowa</Row>
            <Row className='h6'>Cedar Falls, IA</Row>
            <ListGroup variant="flush">
                <ListGroup.Item>
                <Row className='h4'>Drupal Developer</Row>
                <Row className='h5'>August 2015-December 2016</Row>
                </ListGroup.Item>
            </ListGroup>
        </ListGroup.Item>
    </ListGroup>
)

export default ExperienceSection