import React from 'react'
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'

const LinksSection = () => (
    <ListGroup variant="flush">
        <ListGroup.Item>
            <Button variant="link" href='https://gitlab.com/radloffl'>gitlab.com/radloffl</Button>
        </ListGroup.Item>
        <ListGroup.Item>
            <Button variant="link" href='https://www.linkedin.com/in/logan-radloff/'>linkedin.com/in/logan-radloff</Button>
        </ListGroup.Item>
        <ListGroup.Item>
            <Button variant="link" href='https://www.insightapp.net'>insightapp.net</Button>
        </ListGroup.Item>
    </ListGroup>
)

export default LinksSection