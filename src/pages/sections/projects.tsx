import React from 'react'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'

const ProjectsSection = () => (
    <ListGroup variant="flush">
        <ListGroup.Item>
        <Row className='h3'>Madison Roots</Row>
        </ListGroup.Item>
        <ListGroup.Item>
        <Row className='h3'>Insight: Observation Timer</Row>
        </ListGroup.Item>
    </ListGroup>
)

export default ProjectsSection