import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'

import SkillsSection from './sections/skills'
import ExperienceSection from './sections/experience'
import ProjectSection from './sections/projects'
import LinksSection from './sections/links'
import ReferencesSection from './sections/references'
import EducationSection from "./sections/education"

const IndexPage = () => (
  <Layout>
    <SEO title="Logan Radloff" />
    <Row>
      <Col lg={8} md={12}>
        <ListGroup variant="flush">
          <ListGroup.Item>
            <Row className='h2'>Skills</Row>
            <SkillsSection />
          </ListGroup.Item>
          <ListGroup.Item>
            <Row className='h2'>Experience</Row>
            <ExperienceSection />
          </ListGroup.Item>
          <ListGroup.Item>
            <Row className='h2'>Projects</Row>
            <ProjectSection />
          </ListGroup.Item>
        </ListGroup>
        <hr className="d-lg-none"></hr>
      </Col>
      <Col>
        <ListGroup variant="flush">
          <ListGroup.Item>
              <Row className='h2'>Links</Row>
              <LinksSection />
            </ListGroup.Item>
          <ListGroup.Item>
            <Row className='h2'>Education</Row>
            <EducationSection />
          </ListGroup.Item>
          <ListGroup.Item>
            <Row className='h2'>References</Row>
            <ReferencesSection />
          </ListGroup.Item>
        </ListGroup>  
      </Col>
    </Row>
  </Layout>
)

export default IndexPage
