import React from "react"
import Row from 'react-bootstrap/Row'
import Alert from 'react-bootstrap/Alert'
import Container from 'react-bootstrap/Container'

const Footer = () => (
    <Container>
        <hr></hr>
        <Row className="justify-content-center">
            <Alert variant="light">
                Built with{' '}
                <Alert.Link href="https://www.gatsbyjs.com">Gatsby</Alert.Link>,{' '}
                <Alert.Link href="https://reactjs.org">React</Alert.Link>,{' '}
                <Alert.Link href="https://getbootstrap.com">Bootstrap</Alert.Link>,{' & '}
                <Alert.Link href="https://docs.gitlab.com/ee/user/project/pages/">GitLab Pages</Alert.Link>{' '}
                (<a href='https://gitlab.com/radloffl/radloff.dev'>see repository</a>).
            </Alert>
        </Row>
    </Container>
)

export default Footer