import PropTypes from "prop-types"
import React from "react"
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'

const Header = ({ siteTitle }) => (
  <header className="bg-primary">
    <Container>
      <Navbar variant='dark' bg="primary">
        <Nav className="me-auto flex-column">
          <Navbar.Brand href="/">
            {siteTitle}
          </Navbar.Brand>
          <Navbar.Text>
            <small className="text-light">Software Developer</small>
          </Navbar.Text>
        </Nav>
        <Nav className="flex-column">
          <Nav.Item className="ms-auto">
            <Nav.Link active href="mailto:logan@radloff.dev">
              logan@radloff.dev
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="ms-auto">
            <Nav.Link active href="tel:+15634227047">
              (563) 422-7047
            </Nav.Link>
          </Nav.Item>
        </Nav>
      </Navbar>
    </Container>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
